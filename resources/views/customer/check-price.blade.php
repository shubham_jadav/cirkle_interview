@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <form class="form-inline">
          <div class="form-group mb-2">
            <label for="price" class="sr-only">Password</label>
            <input type="number" name="price" class="form-control mr-2" id="price" placeholder="Enter amount">
          </div>
          <button type="button" id="calculate" class="btn btn-primary mb-2">Confirm identity</button>      
        </form>
        </div>
        <div class="col-md-4"></div>
        <div class="col-md-3">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="inputGroup-sizing-default">Final amount</span>
            </div>
            <input type="text" name="finalAmount" disabled class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
          </div>
        </div>
        <div class="col-md-9"></div>
    </div>
</div>
@endsection

@section('page-script')
  <script>
    $( "#calculate" ).click(function() {
      var price = $('input[name="price"]').val();
      if (price == '' || price == null) {
        alert('Please Enter Amount!');
      }else{
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
          }
        });
        $.ajax({
          url: "{{ route('calculate-price') }}",
          method: 'post',
          data: {
            price: price,
          },
          success: function(amount){
            $('input[name="finalAmount"]').val(amount);
          }
        });
      }
    });
  </script>
@endsection