@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
          @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
          @endif

          @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
          @endif
          <div class="col-xs-6">
              <a role="button" href="{{ route('create-range') }}" class="btn btn-primary">Add Range</a>
          </div>
          <div class="card">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">Sr. No</th>
                  <th scope="col">Min Value</th>
                  <th scope="col">Max Value</th>
                  <th scope="col">Percentage</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>
                @forelse($priceRanges as $key => $priceRange)
                  <tr>
                    <th scope="row">{{ $key+1 }}</th>
                    <td>{{ $priceRange->min_value }}</td>
                    <td>{{ $priceRange->max_value }}</td>
                    <td>{{ $priceRange->percentage }}%</td>
                    <td>
                      <a type="button" href="{{ route('edit-range', Crypt::encrypt($priceRange->id)) }}" class="btn btn-info">Edit</a>
                    </td>
                  </tr>
                @empty
                  <tr>
                    <td colspan="5" align="center"> No record found</td>              
                  </tr>
                @endforelse
              </tbody>
              </table>
          </div>
        </div>
    </div>
</div>
@endsection
