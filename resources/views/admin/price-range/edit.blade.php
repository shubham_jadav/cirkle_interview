@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
          <form method="post" action="{{ route('update-range') }}">
            @csrf
            <input type="hidden" name="id" value="{{ $priceRange->id }}">
            <div class="form-group">
              <label for="min_value">Min value:</label>
              <input type="number" name="min_value" id="min_value" value="{{ old('min_value') ? old('min_value') : $priceRange->min_value }}" class="form-control">
              @if ($errors->has('min_value'))
                <span class="text-danger">{{ $errors->first('min_value') }}</span>
              @endif
            </div>
            <div class="form-group">
              <label for="max_value">Max value:</label>
              <input type="number" name="max_value" id="max_value" value="{{ old('max_value') ? old('max_value') : $priceRange->max_value }}" class="form-control">
              @if ($errors->has('max_value'))
                <span class="text-danger">{{ $errors->first('max_value') }}</span>
              @endif
            </div>
            <div class="form-group">
              <label for="percentage">Percentage</label>
              <input name="percentage" id="percentage" value="{{ old('percentage') ? old('percentage') : $priceRange->percentage }}" class="form-control">
              @if ($errors->has('percentage'))
                <span class="text-danger">{{ $errors->first('percentage') }}</span>
              @endif
            </div>
            <input type="submit" class="btn btn-primary">
          </form>
        </div>
    </div>
</div>
@endsection
