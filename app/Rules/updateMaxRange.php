<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\PriceRange;
use Session;

class updateMaxRange implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($value) {

            $nextRange = PriceRange::where('id', '>', Session::get('range_id'))->orderBy('id','desc')->first();

            if ($value == $nextRange->min_value - 1) {
                return true;
            }elseif ($value < $nextRange->min_value || $value >= $nextRange->max_value - 1) {
                return false;
            }elseif($value > $nextRange->min_value){
                Session::put('edit_next_record', true);
            }
            return true;

        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute is not valid.';
    }
}
