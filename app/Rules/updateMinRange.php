<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\PriceRange;
use Session;

class updateMinRange implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $value = (int)$value;

        if ($value) {

            $previousRange = PriceRange::where('id', '<', Session::get('range_id'))->orderBy('id','desc')->first();

            if ($value == $previousRange->max_value + 1) {
                return true;
            }elseif ($value > $previousRange->max_value || $value <= $previousRange->min_value + 1) {
                return false;
            }elseif($value < $previousRange->max_value){
                Session::put('edit_recent_record', true);
            }
            return true;

        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The validation error message.';
    }
}
