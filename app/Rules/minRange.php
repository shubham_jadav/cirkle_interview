<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\PriceRange;
use Session;

class minRange implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $value = (int)$value;

        if (PriceRange::count() == 0 && $value == 1) {
            return true;
        }else{
            if (PriceRange::count() > 0) {

                $latestRange = PriceRange::latest()->first();

                if ($value == $latestRange->max_value + 1) {
                    return true;
                }elseif ($value > $latestRange->max_value || $value <= $latestRange->min_value + 1) {
                    return false;
                }elseif($value < $latestRange->max_value){
                    Session::put('edit_recent_record', true);
                }
                return true;
            }
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute is not valid.';
    }
}
