<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PriceRange;

class CustomerController extends Controller
{
    public function checkPrice()
    {
        return view('customer.check-price');
    }

    public function calculatePrice(Request $request)
    {
        $priceRanges = PriceRange::orderBy('min_value')->get();

        $finalAmount = 0;
        $remainingPrice = null;

        foreach ($priceRanges as $priceRange) {
            $averagePrice = $priceRange->max_value - $priceRange->min_value; 
            
            $remainingPrice = $request->price - $averagePrice;

            $finalAmount += ($request->price / $priceRange->percentage);
            if ($remainingPrice < 1) {
                break;
            }
        }
        return number_format((float)$finalAmount,2,'.','');
    }
}
