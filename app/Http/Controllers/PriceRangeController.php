<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PriceRange;
use App\Rules\minRange;
use App\Rules\updateMinRange;
use App\Rules\updateMaxRange;
use Session;

class PriceRangeController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['priceRanges'] = PriceRange::orderBy('min_value')->get();
        return view('admin.price-range.index',$data);
    }

    public function create()
    {
    	return view('admin.price-range.create');
    }

    public function addRange(Request $request)
    {
    	$request->validate([
            'min_value'  => ['required', 'integer', 'min:1', 'lt:max_value', new minRange],
            'max_value'  => 'required|integer|gt:min_value',
            'percentage' => 'required|numeric|between:0,100.00',
        ]);

        if (Session::get('edit_recent_record')) {
        	$latestRange = PriceRange::latest()->first();
        	$latestRange->max_value = $request->min_value -1;
            $latestRange->save();
            Session::forget('edit_recent_record');
        }

        if (Session::get('delete_recent_record')) {
        	$latestRange = PriceRange::latest()->delete();
            Session::forget('delete_recent_record');
        }
        
        $priceRange =  new PriceRange();

        $priceRange->min_value =  $request->min_value;
        $priceRange->max_value =  $request->max_value;
        $priceRange->percentage =  $request->percentage;

        if ($priceRange->save()) {
	    	return redirect()->route('home')->with('success','Price Range Added Successfully.');
        }else{
	    	return redirect()->route('home')->with('error','Something Went Wrong!');
        }

    }

    public function editRange(Request $request)
    {
    	$id = decrypt($request->id);

    	$data['priceRange'] = PriceRange::find($id);

    	return view('admin.price-range.edit',$data);
    }
    
    public function updateRange(Request $request)
    {
    	Session::put('range_id', $request->id);

    	$request->validate([
            'min_value' => ['required', 'integer', 'min:1', 'lt:max_value', new updateMinRange],
            'max_value' => ['required', 'integer', 'gt:min_value', new updateMaxRange],
            'percentage' => 'required|numeric|between:0,100.00'
        ]);

        Session::forget('range_id');

        if (Session::get('edit_recent_record')) {
        	$previousRange = PriceRange::where('id', '<', $request->id)->orderBy('id','desc')->first();
        	$previousRange->max_value = $request->min_value -1;
            $previousRange->save();
            Session::forget('edit_recent_record');
        }

        if (Session::get('edit_next_record')) {
        	$nextRange = PriceRange::where('id', '>', $request->id)->orderBy('id','desc')->first();
        	$nextRange->min_value = $request->max_value + 1;
            $nextRange->save();
            Session::forget('edit_next_record');
        }

        $priceRange =  PriceRange::find($request->id);

        $priceRange->min_value =  $request->min_value;
        $priceRange->max_value =  $request->max_value;
        $priceRange->percentage =  $request->percentage;

        if ($priceRange->save()) {
	    	return redirect()->route('home')->with('success','Price Range Updated Successfully.');
        }else{
	    	return redirect()->route('home')->with('error','Something Went Wrong!');
        }
    }
}
