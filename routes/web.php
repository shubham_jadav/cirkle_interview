<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::get('/home', 'PriceRangeController@index')->name('home')->middleware('auth');
Route::get('/create-range', 'PriceRangeController@create')->name('create-range')->middleware('auth');
Route::post('/add-range', 'PriceRangeController@addRange')->name('add-range')->middleware('auth');
Route::get('/edit-range/{id}', 'PriceRangeController@editRange')->name('edit-range')->middleware('auth');
Route::post('/update-range', 'PriceRangeController@updateRange')->name('update-range')->middleware('auth');

Route::get('/check-price', 'CustomerController@checkPrice')->name('check-price');
Route::post('/calculate', 'CustomerController@calculatePrice')->name('calculate-price');